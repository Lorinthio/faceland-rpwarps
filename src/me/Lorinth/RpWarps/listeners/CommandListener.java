package me.Lorinth.RpWarps.listeners;

import me.Lorinth.RpWarps.PlayerWarpProfile;
import me.Lorinth.RpWarps.RpWarpsMain;
import me.Lorinth.RpWarps.utilities.WarpUtils;
import me.Lorinth.RpWarps.windows.WarpWindow;

import static me.Lorinth.RpWarps.RpWarpsMain.PROFILES;

import java.sql.ResultSet;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandListener implements CommandExecutor {

	private final RpWarpsMain plugin;
	private WarpUtils warpUtils;

	public CommandListener(RpWarpsMain plugin) {
		this.plugin = plugin;
		this.warpUtils = plugin.warpUtils;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if (sender instanceof Player) {
			Player p = (Player) sender;

			if (cmd.getName().equalsIgnoreCase("quickwarp")) {

				if(args.length == 0){
					commandHelp(p, commandLabel);
					return true;
				} else if (args[0].equalsIgnoreCase("debug")) {
					if(p.hasPermission("LRWarps.debug")) {
						Debug(p, commandLabel, args);
						return true;
					}
				} else if (args[0].equalsIgnoreCase("admin")) {
					if(p.hasPermission("LRWarps.admin")) {
						Admin(p, commandLabel, args);
						return true;
					}
				} else if (args[0].equalsIgnoreCase("name")) {
					return name(p, args, false);
				} else if (args[0].equalsIgnoreCase("icon")) {
					return icon(p, args, false);
				} else if (args[0].equalsIgnoreCase("remove")) {
					return remove(p, args, false);
				} else if (args[0].equalsIgnoreCase("ban")) {
					return ban(p, args, false);
				} else if (args[0].equalsIgnoreCase("unban")) {
					return unban(p, args, false);
				} else if(p.hasPermission("LRWarps.virtual") && args[0].equalsIgnoreCase("virtual")){
					WarpWindow win = new WarpWindow(plugin, p, p);
					RpWarpsMain.winMan.addMainWindow(p, win);
				} else {
					commandHelp(p, commandLabel);
					return true;
				}
			}
			//not from players
		} else {
			if(args[0].equalsIgnoreCase("menu")) {
				return menu(args);
			}
		}
		return true;
	}

	boolean name(Player p, String[] args, boolean AdminOverride) {
		if (args.length < 1) {
			p.sendMessage(ChatColor.RED + "USAGE: /quickwarp name <name> ");
		}

		String name = "";
		for (int i = 1; i<args.length; i++) {
			name = name + args[i] + " ";
		}
		warpUtils.renameWarp(p, name, AdminOverride);
		return true;
	}

	boolean icon(Player p, String[] args, boolean AdminOverride) {
		warpUtils.changeIcon(p, AdminOverride);
		return true;
	}

	boolean ban(Player p, String[] args, boolean AdminOverride) {
		if (args.length < 2) {
			p.sendMessage(ChatColor.RED + "USAGE: /quickwarp ban <player>");
			return true;
		}
		if (args[1].equalsIgnoreCase("all") && !AdminOverride) {
			if (args.length > 2) {
				warpUtils.banFromUserWarps(p, args[2]);
				return true;
			} else {
				p.sendMessage(ChatColor.RED + "USAGE: /quickwarp ban all <player>");
				return true;
			}
		} else {
			warpUtils.banFromWarp(p, args[1], AdminOverride);
		}
		return true;
	}

	boolean unban(Player p, String[] args, boolean AdminOverride) {
		if (args.length > 1) {
			if (args[1].equalsIgnoreCase("all") && !AdminOverride) {
				if (args.length > 2) {
					warpUtils.unbanFromUserWarps(p, args[2]);
				} else {
					p.sendMessage(ChatColor.RED + "USAGE: /quickwarp unban all <player>");
				}
			} else {
				warpUtils.unbanFromWarp(p, args[1], AdminOverride);
			}
		} else {
			p.sendMessage(ChatColor.RED + "USAGE: /quickwarp unban <player>");
		}
		return true;
	}


	boolean remove(Player p, String[] args, boolean AdminOverride) {
		warpUtils.removeWarp(p, AdminOverride);
		return true;
	}

	boolean menu(String[] args) {
		if(args.length > 1) {
			WindowtoOtherUser(args[1]);
			return true;
		}
		return true;
	}

	void seeBanned(Player p) {
		warpUtils.printBanned(p);
	}

	void WindowtoOtherUser(String name){
		Player player = plugin.getServer().getPlayer(name);
		WarpWindow win = new WarpWindow(plugin, player, player);
		RpWarpsMain.winMan.addMainWindow(player, win);
	}

	void Admin (Player player, String commandLabel, String[] args) {
		if(args.length > 1) {
			if(args[1].equalsIgnoreCase("name")) {
				name(player, args, true);
			} 
			else if(args[1].equalsIgnoreCase("icon")) {
				icon(player, args, true);
			}
			else if(args[1].equalsIgnoreCase("ban")) {
				ban(player, args, true);
			}
			else if(args[1].equalsIgnoreCase("unban")) {
				unban(player, args, true);
			}
			else if(args[1].equalsIgnoreCase("seeBanned")) {
				seeBanned(player);
			}
			else if(args[1].equalsIgnoreCase("remove")) {
				remove(player, args, true);
			}
			else if(args[1].equalsIgnoreCase("menu")) {
				menu(args);
			} else {
				AdminHelp(player, commandLabel);
			}
			return;
		}
		AdminHelp(player, commandLabel);
	}

	void AdminHelp(Player p, String commandLabel) {
		String start = ChatColor.GREEN + "Use " + ChatColor.WHITE + "/" + commandLabel + " admin ";

		p.sendMessage(start + "name <name> " + ChatColor.GREEN + "to name your new warp");
		p.sendMessage(start + "icon " + ChatColor.GREEN + "to change the warp's icon");
		p.sendMessage(start + "ban <player>" + ChatColor.GREEN + "to ban a user from a warp");
		p.sendMessage(start + "unban <player> " + ChatColor.GREEN + "to unban a user from a warp");
		p.sendMessage(start + "seeBanned " + ChatColor.GREEN + "to see players banned from a warp");
		p.sendMessage(start + "remove " + ChatColor.GREEN + "to delete a warp");
		p.sendMessage(start + "menu <player> " + ChatColor.GREEN + "to open that player's warp menu");
	}

	void commandHelp(Player p, String commandLabel) {
		String start = ChatColor.GREEN + "Use " + ChatColor.WHITE + "/" + commandLabel + " ";

		p.sendMessage(start + "name <name> " + ChatColor.GREEN + "to name your new warp");
		p.sendMessage(start + "icon " + ChatColor.GREEN + "to change the warp's icon");
		p.sendMessage(start + "ban <player>" + ChatColor.GREEN + "to ban a user from a warp");
		p.sendMessage(start + "unban <player> " + ChatColor.GREEN + "to unban a user from a warp");
		p.sendMessage(start + "remove " + ChatColor.GREEN + "to delete a warp");
		p.sendMessage(start + "ban all <player>" + ChatColor.GREEN + "to ban a user from all of your warps");
		p.sendMessage(start + "unban all <player> " + ChatColor.GREEN + "to unban a user from all of your warps");

		if(p.hasPermission("LRWarps.virtual")){
			p.sendMessage(start + "virtual" + ChatColor.GREEN + "to open the warp menu");
		}
		if (p.hasPermission("LRWarps.admin")) {
			p.sendMessage(ChatColor.GOLD + "Use " + ChatColor.WHITE + "/" + commandLabel + " admin <command> " + ChatColor.GOLD + "to use admin commands");
		}
		if (p.hasPermission("LRWarps.debug")) {
			p.sendMessage(ChatColor.GOLD + "Use " + ChatColor.WHITE + "/" + commandLabel + " debug <mode>" + ChatColor.GOLD + " for debugging");
		}
	}

	void debugHelp(Player p, String commandLabel) {
		String start = ChatColor.GREEN + "Use " + ChatColor.WHITE + "/" + commandLabel + " debug ";

		p.sendMessage(start + "seePlayerWarps <name> " + ChatColor.GREEN + "open another player's warp window");
		p.sendMessage(start + "showWarps <warps owner> <window viewer>  " + ChatColor.GREEN + "to see the id's of your saved warps");
		p.sendMessage(start + "printlist " + ChatColor.GREEN + "to see the id's of your saved warps");
		p.sendMessage(start + "icon " + ChatColor.GREEN + "gets a warp's icon");
		p.sendMessage(start + "save " + ChatColor.GREEN + "force save your data and see the SQL sent");
		p.sendMessage(start + "reload " + ChatColor.GREEN + "reloads all warp data (not player data)");
	}

	void Debug (Player player, String commandLabel , String[] args) {

		if(args.length > 1) {
			
			if(args[1].equalsIgnoreCase("icon")) {
				player.getWorld().dropItem(player.getLocation(), warpUtils.getLookedAtWarp(player, true).getDisplayItem());
				return;
			}
			
			//see another player's warp window
			if(args[1].equalsIgnoreCase("seePlayerWarps")) {
				if(args.length == 2) {
					debugHelp(player, commandLabel);
				} else {
					Player window_player = plugin.getServer().getPlayer(args[2]);
					if(window_player != null) {
						WarpWindow win = new WarpWindow(plugin, player, window_player);
						WarpWindow win = new WarpWindow(plugin, window_player, player);
						RpWarpsMain.winMan.addMainWindow(player, win);
					} else {
						player.sendMessage(ChatColor.RED + "Player " + ChatColor.WHITE + args[2] + ChatColor.RED + " not found!");
						return;
					}
					return;
				}
			}

			//see your warp list (text)
			if(args[1].equalsIgnoreCase("printList")) {
				PlayerWarpProfile profile = PROFILES.get(player);
				if(profile == null) {
					player.sendMessage("no profile found!");
				} else {
					if(profile.getKnownWarps() == null) {
						player.sendMessage("No known warps");
					} else {
						player.sendMessage(profile.getKnownWarps().toString());
					}
				}
				return;
			}

			if (args[1].equalsIgnoreCase("save")) {
				PlayerWarpProfile profile = PROFILES.get(player);
				profile.save(true);
			} if (args[1].equalsIgnoreCase("reload")) {
				return;
			} 
			if (args[1].equalsIgnoreCase("reload")) {
				ResultSet PlayerWarpsRS = plugin.SQL.get(RpWarpsMain.tableWarps);
				warpUtils.getPlayerOwnedWarps(PlayerWarpsRS);
				return;
			}
			
			
			if(args.length > 2) {
				if(args[1].equalsIgnoreCase("showWarps")) {
					Player window_player = plugin.getServer().getPlayer(args[2]);
					Player window_viewer = player;
					if(args.length > 3 ) {
						window_viewer = plugin.getServer().getPlayer(args[3]);
					}
					if(window_player != null) {
						if(window_viewer != null) {
							WarpWindow win = new WarpWindow(plugin, window_viewer, window_player);
							RpWarpsMain.winMan.addMainWindow(player, win);
							WarpWindow win = new WarpWindow(plugin, window_player, window_viewer);
							RpWarpsMain.winMan.addMainWindow(window_viewer, win);
						} else {
							player.sendMessage(ChatColor.RED + "Player " + ChatColor.WHITE + args[3] + ChatColor.RED + " not found!");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Player " + ChatColor.WHITE + args[2] + ChatColor.RED + " not found!");
					}
					return;
				}
			}
			
		}
		debugHelp(player, commandLabel);
	}
}
