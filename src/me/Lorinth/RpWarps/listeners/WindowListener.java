package me.Lorinth.RpWarps.listeners;

import me.Lorinth.RpWarps.RpWarpsMain;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class WindowListener implements Listener {

	@EventHandler
	public void OnPlayerClickInventory(InventoryClickEvent e) {
		if (!(e.getWhoClicked() instanceof Player)) {
			return;
		}
		RpWarpsMain.winMan.handleClickEvent(e);
	}

	@EventHandler
	public void OnWindowClose(InventoryCloseEvent e) {
		if (!(e.getPlayer() instanceof Player)) {
			return;
		}
		RpWarpsMain.winMan.removePlayer((Player) e.getPlayer()); 
	}
}
