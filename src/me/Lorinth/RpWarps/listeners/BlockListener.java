package me.Lorinth.RpWarps.listeners;

import static me.Lorinth.RpWarps.RpWarpsMain.PROFILES;

import me.Lorinth.RpWarps.RpWarpsMain;
import me.Lorinth.RpWarps.Warp;
import me.Lorinth.RpWarps.windows.WarpWindow;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

public class BlockListener implements Listener {

  private final RpWarpsMain plugin;

  public BlockListener(RpWarpsMain plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onBlockPlace(BlockPlaceEvent event){
    if (event.isCancelled()) {
      return;
    }

    Player player = event.getPlayer();
    if (event.getBlock().getType() == Material.SEA_LANTERN) {
      Location loc = event.getBlock().getLocation();

      boolean one = loc.clone().add(1, 0, 0).getBlock().getType() == Material.DOUBLE_STEP;
      boolean two = loc.clone().add(-1, 0, 0).getBlock().getType() == Material.DOUBLE_STEP;
      boolean three = loc.clone().add(0, 0, 1).getBlock().getType() == Material.DOUBLE_STEP;
      boolean four = loc.clone().add(0, 0, -1).getBlock().getType() == Material.DOUBLE_STEP;

      if (one && two && three && four) {
        plugin.warpUtils.createWarp(player, loc);
      }
    }
  }

  @EventHandler
  public void onBlockDestroy(BlockBreakEvent event){
    if (event.isCancelled()) {
      return;
    }

    if (event.getBlock().getType() == Material.SEA_LANTERN) {
      Block b = event.getBlock();
      Warp warp = plugin.warpUtils.getLocWarp(b);
      if (warp == null) {
        return;
      }
      if (warp.hasEditRights(event.getPlayer(), true)) {
        plugin.warpUtils.removeWarp(event.getPlayer(), false);
      } else {
        event.setCancelled(true);
      }
    }
  }

  @EventHandler
  public void onBlockRightClick(PlayerInteractEvent event) {
    if (event.getHand() != EquipmentSlot.OFF_HAND) {
      return;
    }
    if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
      return;
    }
    Player p = event.getPlayer();
    Warp warp = plugin.warpUtils.getLookedAtWarp(p, false);

    if (warp != null) {
      if (!warp.getBannedUsers().contains(p.getUniqueId().toString())) {
        PROFILES.get(p).addWarp(warp);
        WarpWindow win = new WarpWindow(plugin, p, p);
        RpWarpsMain.winMan.addMainWindow(p, win);
      } else {
        p.sendMessage(ChatColor.RED + "You cannot learn this warp.");
      }

      event.setCancelled(true);
    }
  }
}
