package me.Lorinth.RpWarps.listeners;

import static me.Lorinth.RpWarps.RpWarpsMain.PROFILES;

import me.Lorinth.RpWarps.PlayerWarpProfile;
import me.Lorinth.RpWarps.RpWarpsMain;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EntryExitListener implements Listener {

  private final RpWarpsMain plugin;

  public EntryExitListener(RpWarpsMain plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void OnPlayerConnect(PlayerJoinEvent event){
    PlayerWarpProfile profile = new PlayerWarpProfile(plugin, event.getPlayer());
    PROFILES.put(event.getPlayer(), profile);
  }

  @EventHandler
  public void OnPlayerDisconnect(PlayerQuitEvent event){
	  leaveEvent(event.getPlayer());
  }

  @EventHandler
  public void OnPlayerKicked(PlayerKickEvent event){
	  leaveEvent(event.getPlayer());
  }
  
  private void leaveEvent(Player player) {
	    PlayerWarpProfile profile = PROFILES.get(player);
	    profile.save(false);
	    PROFILES.remove(player);
	    if(RpWarpsMain.PERMISSIONS.containsKey(player)) {
	    	RpWarpsMain.PERMISSIONS.remove(player);
	    }
  }
}
