package me.Lorinth.RpWarps;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Warp {

	private short Durability;
	private String ID;
	private String name;
	private Material material = Material.ENDER_PEARL;
	private Location loc;
	private String owner; //UUID
	private ArrayList<String> bannedUsers = new ArrayList<>();

	private ItemStack displayItem;

	public Warp() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return ID;
	}

	public void setId(String id) {
		this.ID = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner.getUniqueId().toString();
	}

	public void setOwner(String uuid) {
		this.owner = uuid;
	}

	public Location getLocation() {
		return loc;
	}

	public void setLocation(Location loc) {
		this.loc = loc;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public String getDurabilityString() {
		String retVal;
		retVal = "" + Durability;
		return retVal;
	}

	public short getDurability() {
		return Durability;
	}

	public void setDurability(short durability) {
		this.Durability = durability;
	}

	public ArrayList<String> getBannedUsers() {
		return bannedUsers;
	}

	public void setBannedUsers(ArrayList<String> uuids) {
		this.bannedUsers = uuids;
	}

	public void addBannedUser(String uuid) {
		this.bannedUsers.add(uuid);
	}

	public ItemStack getDisplayItem() {
		return displayItem;
	}

	public boolean hasEditRights(Player p, boolean outputText){

		if (owner.equalsIgnoreCase(p.getUniqueId().toString())) {
			return true;
		}
		if(outputText) {
			p.sendMessage(ChatColor.RED + "You cannot edit this warp point!");
		}
		return false;
	}

	public void makeDisplayItem() {
		if (material == Material.AIR) {
			material = Material.ENDER_PEARL;
		}
		displayItem = new ItemStack(material);
		ItemMeta meta = displayItem.getItemMeta();
		meta.setDisplayName(name);
		List<String> lore = new ArrayList<String>();
		{
			OfflinePlayer p = Bukkit.getOfflinePlayer(UUID.fromString(owner));
			lore.add(ChatColor.WHITE + "Owner: " + ChatColor.GRAY + p.getName());
			lore.add(ChatColor.WHITE + "World: " + ChatColor.GRAY + loc.getWorld().getName());
			lore.add(ChatColor.WHITE + "Location: "  + ChatColor.GRAY + "("  +loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ() + ")");
		}

		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		displayItem.setItemMeta(meta);
		if(Durability > 0) {
			displayItem.setDurability(Durability);
		}
	}

	@Override
	public String toString() {
		String line;
		line = "PlayerWarp {ID: " + ID + ", Name: " + ChatColor.stripColor(name) + ", Material: " + material.toString() + ", Owner: " + owner + ", Location:" + loc.toString() + "}";
		return line;
	}


	public void teleport(Player p){
		p.teleport(loc.clone().add(0.5, 1, 0.5));
	}

	public void AddBannedUser(Player banned){
		this.bannedUsers.add(banned.getUniqueId().toString());
	}

	public void RemoveBannedUser(Player unbanned){
		this.bannedUsers.remove(unbanned.getUniqueId().toString());
	}
}
