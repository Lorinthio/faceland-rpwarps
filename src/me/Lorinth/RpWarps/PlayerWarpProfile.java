package me.Lorinth.RpWarps;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.ccgreen.SQLlib.SQLlibMain;

public class PlayerWarpProfile {

	private List<String> knownWarps = new ArrayList<String>();
	private static SQLlibMain SQL;
	private Player player;

	public PlayerWarpProfile(RpWarpsMain main, Player player) {
		this.player = player;
		PlayerWarpProfile.SQL = main.SQL;
		String uuid = player.getUniqueId().toString();
		ResultSet warps = SQL.get(RpWarpsMain.tableData, "uuid = '" + uuid + "'");
		try {
			if(warps.next()) {
				String warpString = warps.getString("warps");
				if(warpString.startsWith("[")) {
					warpString = warpString.replace("[", "");
					warpString = warpString.replace("]", "");
					warpString = warpString.replace(" ", "");
				}
				String[] warpsKnown = warpString.split(",");
				for(int i = 0; i < warpsKnown.length; i++) {
					knownWarps.add(warpsKnown[i]);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (main.warpUtils.noWarps()) {
			for(String id : knownWarps) {
				if(main.warpUtils.getWarp(id) == null) {
					knownWarps.remove(id);
				}
			}
		}
	}

	public void save(boolean DebugOverride) {
		if(!knownWarps.isEmpty()) {
			if(DebugOverride) {
				player.sendMessage(RpWarpsMain.tableData + "|uuid, warps" + "|" + player.getUniqueId() + "', '" + String.join(",", knownWarps) + "'");
			}
			SQL.set(RpWarpsMain.tableData, "uuid, warps", "'" + player.getUniqueId() + "', '" + String.join(",", knownWarps) + "'");
		} else {
			if(player.hasPermission("LRWarps.debugSeeSQL")) {
				player.sendMessage("No Known Warps!");
			}
		}
	}

	public void addWarp(Warp w) {

		if (!knownWarps.contains(w.getId())) {
			int maxWarps = 45;

			if (knownWarps.size() < maxWarps) {
				knownWarps.add(w.getId());
				player.sendMessage(ChatColor.GREEN + "This location has been memorized and added to your list!");
				player.sendMessage(ChatColor.GREEN + "You now have access to " + w.getName() + ChatColor.GREEN + "!");
			} else {
				player.sendMessage(ChatColor.RED + "You have memorized the maximum number of locations!");
			}
		}
	}

	public List<String> getKnownWarps() {
		return knownWarps;
	}

}