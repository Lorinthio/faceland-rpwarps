package me.Lorinth.RpWarps.utilities;

import static me.Lorinth.RpWarps.RpWarpsMain.CONSOLE;

import org.bukkit.ChatColor;

public class StringUtils {

  public static void printInfo(String line){
    CONSOLE.sendMessage(ChatColor.GREEN + "[RpWarps] : " + line);
  }

  public static void printWarning(String line){
    CONSOLE.sendMessage(ChatColor.YELLOW + "[RpWarps] : " + line);
  }

  public static void printError(String line){
    CONSOLE.sendMessage(ChatColor.RED + "[RpWarps] : " + line);
  }

  public static String convertToMColors(String line){
    return line.replaceAll("&", "�");
  }
}
