package me.Lorinth.RpWarps.utilities;

import static me.Lorinth.RpWarps.RpWarpsMain.PROFILES;
import static me.Lorinth.RpWarps.RpWarpsMain.getNextId;
import static me.Lorinth.RpWarps.utilities.StringUtils.convertToMColors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import me.Lorinth.RpWarps.PlayerWarpProfile;
import me.Lorinth.RpWarps.RpWarpsMain;
import me.Lorinth.RpWarps.Warp;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockVector;
import org.bukkit.util.Vector;

public class WarpUtils {

	private static HashMap<String, Warp> PLAYER_WARPS = new HashMap<String, Warp>();
	private static HashMap<World, HashMap<Vector,Warp>> LOCATION_WARPS = new HashMap<World, HashMap<Vector,Warp>>();
	private Set<Material> clearMats = new HashSet<Material>();
	private RpWarpsMain plugin;

	public boolean noWarps() {
		return PLAYER_WARPS.isEmpty();
	}

	public WarpUtils(RpWarpsMain plugin) {
		this.plugin = plugin;
		clearMats.add(Material.AIR);
		clearMats.add(Material.CARPET);
		clearMats.add(Material.GRASS);
		clearMats.add(Material.LONG_GRASS);
		clearMats.add(Material.STATIONARY_WATER);
		clearMats.add(Material.WATER);
		clearMats.add(Material.SIGN);
	}

	public Warp getWarp(String id) {
		return PLAYER_WARPS.get(id);
	}

	public Warp getLocWarp(Block b) {
		BlockVector bv = b.getLocation().toVector().toBlockVector();
		return LOCATION_WARPS.get(b.getWorld()).get(bv);
	}

	public void setupWarpLocations() {

		for(World w : Bukkit.getServer().getWorlds()) {
			LOCATION_WARPS.put(w, new HashMap<>());
		}
	}

	public Warp getLookedAtWarp(Player p, boolean command) {
		Block b = p.getTargetBlock(clearMats, 10);
		if(b.getType() == Material.SEA_LANTERN) {
			Warp warp = LOCATION_WARPS.get(b.getWorld()).get(b.getLocation().toVector().toBlockVector());
			if(warp == null && command) {
				p.sendMessage(ChatColor.RED + "This lantern is not a warp");
			}
			return warp;
		}
		if(command) {
			p.sendMessage(ChatColor.RED + "You must be looking at a warp to use this command");
		}
		return null;

	}

	public void createWarp(Player player, Location loc) {
		HashMap<Vector, Warp> warps = LOCATION_WARPS.get(player.getWorld());
		Vector locVector = loc.toVector().toBlockVector();

		if (warps.containsKey(locVector)){
			player.sendMessage(ChatColor.GREEN + "Warp reactivated! ");
		} else {

			String id = getNextId();
			Warp warp = new Warp();

			warp.setId(id);
			warp.setLocation(loc);
			warp.setOwner(player);
			warp.setName("<Unnamed>");
			warp.setDurability((short) 0);


			player.sendMessage(ChatColor.GREEN + "Warp Created! ");
			player.sendMessage(ChatColor.GREEN + "Use " + ChatColor.WHITE + "/quickwarp name <name> " + ChatColor.GREEN + "to name it");
			player.sendMessage(ChatColor.GREEN + "Use " + ChatColor.WHITE + "/quickwarp icon " + ChatColor.GREEN + "to change the icon");
			if (player.hasPermission("LRWarps.convert")) {
				player.sendMessage(ChatColor.GOLD + "Use " + ChatColor.WHITE + "/quickwarp convert " + ChatColor.GOLD + "to convert this to a server warp");
			} 
			if (player.hasPermission("LRWarps.admin")) {
				player.sendMessage(ChatColor.GOLD + "Use " + ChatColor.WHITE + "/quickwarp setaccess " + ChatColor.GOLD + "to convert this to an access point");
			}
			player.playSound(loc, Sound.BLOCK_PORTAL_TRIGGER, 1, 1);

			warp.makeDisplayItem();

			PROFILES.get(player).addWarp(warp);

			PLAYER_WARPS.put(id, warp);
			warps.put(locVector, warp);
			LOCATION_WARPS.put(player.getWorld(), warps);
			saveWarp(warp);
		}
	}

	public void banFromWarp(Player player, String banned, boolean AdminOverride){
		Player bannedPlayer = Bukkit.getPlayer(banned);
		if (bannedPlayer == null) {
			player.sendMessage(ChatColor.RED + "The player " + banned + " was not found!");
			return;
		}
		Block block = player.getTargetBlock((Set<Material>)null, 10);
		Warp warp = LOCATION_WARPS.get(block.getWorld()).get(block.getLocation().toVector().toBlockVector());
		if (warp == null) {
			player.sendMessage(ChatColor.RED + "That is not a warp!");
			return;
		}
		if (warp.hasEditRights(player, true) || AdminOverride) {
			warp.AddBannedUser(bannedPlayer);
			player.sendMessage(ChatColor.GREEN + "You have banned " + ChatColor.WHITE + bannedPlayer
					.getDisplayName() + ChatColor.GREEN + " from this warp!");
			saveWarp(warp);
		} else {
			player.sendMessage(ChatColor.RED + "You do not own this warp!");
		}
	}

	public void renameWarp(Player p, String name, boolean AdminOverride){
		Warp warp = getLookedAtWarp(p, true);
		if (warp != null) {
			if (warp.hasEditRights(p, true) || AdminOverride) {
				warp.setName(convertToMColors(name));
				warp.makeDisplayItem();

				p.sendMessage(ChatColor.GREEN + "Set the name of this warp to " + warp.getName());
				saveWarp(warp);
			}
		}
	}

	public void unbanFromUserWarps(Player p, String unbanned){
		Player unbannedPlayer = Bukkit.getPlayer(unbanned);
		if(unbannedPlayer != null){
			for(Warp w : PLAYER_WARPS.values()){
				//only look at warps owned by this player
				if(w.hasEditRights(p, false)){
					w.RemoveBannedUser(unbannedPlayer);
					saveWarp(w);
				}
			}
			p.sendMessage(ChatColor.GREEN + "You have unbanned " + ChatColor.WHITE + unbannedPlayer.getDisplayName() +
					ChatColor.GREEN + " from all of your warps!");
		}
		else{
			p.sendMessage(ChatColor.RED + "The player " + unbanned + " was not found!");
		}
	}

	public void unbanFromWarp(Player p, String unbanned, boolean AdminOverride){
		Player bannedPlayer = Bukkit.getPlayer(unbanned);
		if(bannedPlayer != null){
			Warp w = getLookedAtWarp(p, true);
			if(w != null){
				if(w.hasEditRights(p, true) || AdminOverride){
					w.RemoveBannedUser(bannedPlayer);
					p.sendMessage(ChatColor.GREEN + "You have unbanned " + ChatColor.WHITE + bannedPlayer
							.getDisplayName() + ChatColor.GREEN + " from this warp!");
					saveWarp(w);
				}
				else{
					p.sendMessage(ChatColor.RED + "You do not own this warp!");
				}
			}
			else{
				p.sendMessage(ChatColor.RED + "That is not a warp!");
			}
		}
		else{
			p.sendMessage(ChatColor.RED + "The player " + unbanned + " was not found!");
		}
	}

	public void banFromUserWarps(Player p, String banned){
		Player bannedPlayer = Bukkit.getPlayer(banned);
		if(bannedPlayer != null){
			for(Warp w : PLAYER_WARPS.values()){
				//only look at warps owned by this player
				if(w.hasEditRights(p, false)){
					w.AddBannedUser(bannedPlayer);
					saveWarp(w);
				}
			}
			p.sendMessage(ChatColor.GREEN + "You have banned " + ChatColor.WHITE + bannedPlayer.getDisplayName() +
					ChatColor.GREEN + " from all of your warps!");
		}
		else{
			p.sendMessage(ChatColor.RED + "The player " + banned + " was not found!");
		}
	}

	public void printBanned(Player p) {
		Warp w = getLookedAtWarp(p, true);
		if(w == null){
			p.sendMessage(ChatColor.RED + "This is not a warp!");
			return;
		}
		p.sendMessage("Banned users are: " + w.getBannedUsers().toString());
	}

	public void getPlayerOwnedWarps(ResultSet warplist) {

		if(!LOCATION_WARPS.isEmpty()) {
			LOCATION_WARPS = new HashMap<World, HashMap<Vector,Warp>>();
		}
		if(!PLAYER_WARPS.isEmpty()) {
			PLAYER_WARPS = null;
		}

		try {
			while(warplist.next()) {

				Warp warp = new Warp();
				Location loc;
				String world, id;
				double x, y, z;

				id = warplist.getString("id");

				warp.setName(warplist.getString("name"));
				warp.setId(id);
				warp.setOwner(warplist.getString("owner"));
				world = warplist.getString("world");
				world = world.replace("CraftWorld{name=", "");
				world = world.replace("}", "");
				x = warplist.getDouble("x");
				y = warplist.getDouble("y");
				z = warplist.getDouble("z");
				loc = new Location(Bukkit.getWorld(world), x, y, z);
				warp.setLocation(loc);
				warp.setMaterial(Material.getMaterial(warplist.getString("material")));
				warp.setDurability(warplist.getShort("durability"));
				String banned = warplist.getString("banned");
				ArrayList<String> bannedUsers = new ArrayList<String>(Arrays.asList(banned.toString().split(",")));
				warp.setBannedUsers(bannedUsers);

				HashMap<Vector, Warp> warps;
				if(LOCATION_WARPS.containsKey(loc.getWorld())) {
					warps = LOCATION_WARPS.get(loc.getWorld());
				} else {
					warps = new HashMap<Vector, Warp>();
				}
				warps.put(loc.toVector().toBlockVector(), warp);
				LOCATION_WARPS.put(loc.getWorld(), warps);
				PLAYER_WARPS.put(id, warp);
				warp.makeDisplayItem();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void destroyWarp(Warp w){
		for (Player p : Bukkit.getOnlinePlayers()) {
			PlayerWarpProfile profile = PROFILES.get(p);
			profile.getKnownWarps().remove(w.getId());
		}
		plugin.SQL.delete(RpWarpsMain.tableWarps, "id='" + w.getId() + "';");
		PLAYER_WARPS.remove(w.getId());
	}

	public void removeWarp(Player p, boolean AdminOverride) {
		Warp warp = getLookedAtWarp(p, true);
		if(warp != null){
			if (warp.hasEditRights(p, true) || AdminOverride) {
				HashMap<Vector, Warp> warps = LOCATION_WARPS.get(warp.getLocation().getWorld());
				warps.remove(warp.getLocation().toVector().toBlockVector());
				LOCATION_WARPS.put(warp.getLocation().getWorld(), warps);
				destroyWarp(warp);
				p.sendMessage(ChatColor.RED +"Warp Destroyed!");
			}
		}
	}

	public void changeIcon(Player p, boolean AdminOverride){
		Warp w = getLookedAtWarp(p, true);
		if(w != null){
			if(w.hasEditRights(p, true) || AdminOverride){
				try{
					if(p.getEquipment().getItemInMainHand() != null){
						w.setMaterial(p.getEquipment().getItemInMainHand().getType());
						w.setDurability(p.getEquipment().getItemInMainHand().getDurability());
						if (w.getMaterial() == Material.AIR) {
							w.setMaterial(Material.ENDER_PEARL);
						}
						w.makeDisplayItem();

						p.sendMessage(ChatColor.GREEN + "Set the icon of this warp to " + w.getMaterial().toString());
						saveWarp(w);
					}
					else{
						p.sendMessage(ChatColor.RED + "You must be holding an item to set an icon!");
					}
				}
				catch(NullPointerException e){
					p.sendMessage(ChatColor.RED + "You must be holding an item to set an icon!");
				}
			}
			else{
				p.sendMessage(ChatColor.RED + "You are not the owner of that warp!");
			}
		}
	}

	public void saveWarp(Warp warp) {
		//organisation is a little weird here cause of previous setup for serverwarps and access points
		//TODO organise
		String id = warp.getId();
		String world = warp.getLocation().getWorld().toString();
		world = world.replace("CraftWorld{name=", "");
		world = world.replace("}", "");
		int x = warp.getLocation().getBlockX();
		int y = warp.getLocation().getBlockY();
		int z = warp.getLocation().getBlockZ();
		String location = world + "', '" + x + "', '" + y + "', '" + z;

		String owner = warp.getOwner();
		String name = warp.getName();
		name = name.replace("'", "");
		String material = warp.getMaterial().toString();
		short durability = warp.getDurability();
		String values = "'" + id + "', '" + owner + "', '" + name + "', '" + location + "', '" + material + "', '" + durability + "'";
		String banned = warp.getBannedUsers().toString();

		plugin.SQL.set(RpWarpsMain.tableWarps, "id, owner, name, world, x, y, z, material, durability, banned", values + ",' " + banned + "'");
	}
}
