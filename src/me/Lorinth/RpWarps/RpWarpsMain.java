package me.Lorinth.RpWarps;

import static me.Lorinth.RpWarps.utilities.StringUtils.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import me.Lorinth.RpWarps.listeners.BlockListener;
import me.Lorinth.RpWarps.listeners.CommandListener;
import me.Lorinth.RpWarps.listeners.EntryExitListener;
import me.Lorinth.RpWarps.listeners.WindowListener;
import me.Lorinth.RpWarps.utilities.WarpUtils;
import me.Lorinth.RpWarps.windows.WindowManager;
import me.ccgreen.SQLlib.SQLlibMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;

public class RpWarpsMain extends JavaPlugin implements Listener {

	public static ConsoleCommandSender CONSOLE;

	public static HashMap<Player, PlayerWarpProfile> PROFILES = new HashMap<Player, PlayerWarpProfile>();
	public static HashMap<Player, PermissionAttachment> PERMISSIONS = new HashMap<Player, PermissionAttachment>();

	public static int curID = 0;
	private long autosave = 300;

	public File configFile;
	public YamlConfiguration configYml;
	public WarpUtils warpUtils = new WarpUtils(this);
	
	public static WindowManager winMan;
	public SQLlibMain SQL;
	
	private static String tablePrefix = "RpWarps_";
	public static String tableWarps = tablePrefix + "PlayerWarps";
	public static String tableData = tablePrefix + "PlayerData";

	boolean imediateShutdown = false;

	@Override
	public void onEnable() {

		if(Bukkit.getServer().getPluginManager().getPlugin("WarpToSQL") != null) {
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[RpWarps] : Plugin will not run while the updater is installed");
			imediateShutdown = true;
			Bukkit.getServer().getPluginManager().disablePlugin(this);
		} else if(!SQLlibMain.configSetup){
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[RpWarps] : SQL config not setup correctly, disabling RpWarps");
			imediateShutdown = true;
			Bukkit.getServer().getPluginManager().disablePlugin(this);
		} else {
			
			SQL = (SQLlibMain) Bukkit.getServer().getPluginManager().getPlugin("SQLlib");
			getCommand("quickwarp").setExecutor(new CommandListener(this));

			getServer().getPluginManager().registerEvents(new WindowListener(), this);
			getServer().getPluginManager().registerEvents(new EntryExitListener(this), this);
			getServer().getPluginManager().registerEvents(new BlockListener(this), this);
			CONSOLE = Bukkit.getServer().getConsoleSender();

			winMan = new WindowManager(this);
			
			warpUtils.setupWarpLocations();
			loadFiles();

			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

				@Override
				public void run() {
					saveData();
				}

			}, autosave * 20, autosave * 20);

			for (Player p : Bukkit.getOnlinePlayers()) {
				PlayerWarpProfile profile = new PlayerWarpProfile(this, p);
				PROFILES.put(p, profile);
			}

			Bukkit.getPluginManager().registerEvents(this, this);

			initialiseOnlinePlayers();
			
			printInfo("has been enabled!");
		}
	}

	@Override
	public void onDisable(){
		if(!imediateShutdown) {
			winMan.closeInventoryies();
			saveData();
			for(Player p : Bukkit.getOnlinePlayers()) {
				PlayerWarpProfile profile = PROFILES.get(p);
				if(profile != null) {
					profile.save(false);
				}
			}
			printWarning("has been disabled!");
		}
	}

	private void initialiseOnlinePlayers() {
		for(Player player : Bukkit.getServer().getOnlinePlayers()) {
		PlayerWarpProfile profile = new PlayerWarpProfile(this, player);
	    PROFILES.put(player, profile);
		}
	}
	
	private void loadFiles() {		
		
		SQL.initialiseTable(tableWarps, "id", "id MEDIUMINT NOT NULL, owner TINYTEXT NOT NULL, name TEXT NOT NULL, world TINYTEXT NOT NULL, x MEDIUMINT NOT NULL, y SMALLINT NOT NULL, z MEDIUMINT NOT NULL, material TINYTEXT NOT NULL, durability SMALLINT NOT NULL, banned LONGTEXT");
		SQL.initialiseTable(tableData, "uuid", "uuid VARCHAR(37) NOT NULL, warps TEXT");
		
		configFile = new File(getDataFolder(), "config.yml");

		if(!configFile.exists()){
			new File(this.getDataFolder() + "").mkdir();

			try {
				if(configFile.createNewFile()){
					try {
						PrintWriter writer = new PrintWriter(configFile);

						writer.write("#Developers : Lorinthios && ccgreen\n"
								+ "Title: QuickWarp!\n"
								+ "Title Color: '&0'\n"
								+ "CurID: 0\n" 
								+ "Autosave: 300"
								);
						writer.close();
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}

		}

		configYml = YamlConfiguration.loadConfiguration(configFile);
		curID = configYml.getInt("CurID");
		autosave = configYml.getLong("Autosave");

		System.out.println("Loading warps...");
		ResultSet PlayerWarpsRS = SQL.get(tableWarps);
		
		warpUtils.getPlayerOwnedWarps(PlayerWarpsRS);

		System.out.println("Loading complete!");
	}

	private void saveData() {
		printInfo("Saving data...");
		boolean error = false;

		configYml.set("CurID", curID);

		try {
			configYml.save(configFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error = true;
		}

		if (error) {
			printError("Data save, was unsuccessful, check logs");
		} else {
			printInfo("Success!");
		}
	}

	public static String getNextId(){
		String retVal;

		retVal = "" + curID;		
		curID ++;

		return retVal;
	}
}
