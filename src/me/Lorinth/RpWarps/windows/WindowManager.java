package me.Lorinth.RpWarps.windows;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

import me.Lorinth.RpWarps.RpWarpsMain;

public class WindowManager {

	private static HashMap<Player, WarpWindow> WARP_WINDOWS = new HashMap<Player, WarpWindow>();
	private static HashMap<Player, WarpConfirmWindow> PLAYER_CONFIRMS = new HashMap<Player, WarpConfirmWindow>();
	private RpWarpsMain main;

	public WindowManager(RpWarpsMain main){
		this.main = main;
	}


	public void addMainWindow(Player player, WarpWindow WW) {
		WARP_WINDOWS.put(player, WW);
	}

	public void addConfirmWindow(Player player, WarpConfirmWindow WCW) {
		PLAYER_CONFIRMS.put(player, WCW);
	}

	public void removePlayer(Player player) {
		if (WARP_WINDOWS.containsKey(player)) {
			WARP_WINDOWS.remove(player);
		}
		if (PLAYER_CONFIRMS.containsKey(player)) {
			PLAYER_CONFIRMS.remove(player);
		}
	}

	public void handleClickEvent(InventoryClickEvent e) {
		if(e.getClickedInventory() == null) {
			return;
		}
		if (e.getClickedInventory().getType() == InventoryType.CHEST) {
			if (WARP_WINDOWS.containsKey(e.getWhoClicked())){	
				WarpWindow win = WARP_WINDOWS.get(e.getWhoClicked());
				win.HandleClickEvent(e);
				e.setCancelled(true);
			}
			if (PLAYER_CONFIRMS.containsKey(e.getWhoClicked())) {
				WarpConfirmWindow win = PLAYER_CONFIRMS.get(e.getWhoClicked());
				win.handleClickEvent(e);
				e.setCancelled(true);
			}
		}
	}

	public void closeInventoryies() {
		for(Player p : main.getServer().getOnlinePlayers()) {
			p.closeInventory();
			removePlayer(p);
		}
	}
}