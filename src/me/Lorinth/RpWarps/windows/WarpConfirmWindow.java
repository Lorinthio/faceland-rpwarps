package me.Lorinth.RpWarps.windows;

import me.Lorinth.RpWarps.PlayerWarpProfile;
import me.Lorinth.RpWarps.Warp;
import org.bukkit.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class WarpConfirmWindow {

	private Player play;
	private Warp warp;
	private PlayerWarpProfile profile;
	private boolean showDelete;

	public WarpConfirmWindow(Player player, Warp warp, PlayerWarpProfile profile, boolean showDelete){
		this.play = player;
		this.warp = warp;
		this.profile = profile;
		this.showDelete = showDelete;

		makeInventory();
	}

	private void makeInventory() {
		Inventory inv = Bukkit.createInventory(null, 27, ChatColor.BLACK + "" + ChatColor.DARK_GRAY + "♦" + ChatColor.WHITE + warp.getName());

		ItemStack warpItem = new ItemStack(Material.NETHER_STAR);
		ItemMeta meta = warpItem.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Warp Now!");
		warpItem.setItemMeta(meta);

		ItemStack cancelItem = new ItemStack(Material.COAL);
		meta = cancelItem.getItemMeta();
		meta.setDisplayName(ChatColor.YELLOW + "Cancel");
		cancelItem.setItemMeta(meta);

		ItemStack forgetItem = new ItemStack(Material.REDSTONE);
		meta = forgetItem.getItemMeta();
		meta.setDisplayName(ChatColor.RED + "Delete Warp");
		forgetItem.setItemMeta(meta);


		inv.setItem(12, warpItem);
		inv.setItem(14, cancelItem);
		if(showDelete) {
			inv.setItem(26, forgetItem);
		}
		
		play.openInventory(inv);
	}

	public void handleClickEvent(InventoryClickEvent e){
		if (!(e.getInventory().getName().startsWith(ChatColor.BLACK + "" + ChatColor.DARK_GRAY + "♦"))) {
			return;
		}
		if(e.getSlot() == 12){
			if(Bukkit.getServer().getWorlds().contains(warp.getLocation().getWorld())) {
				play.closeInventory();
				warp.teleport(play);
				play.sendMessage(ChatColor.GREEN + "You have warped to " + warp.getName() + ChatColor.GREEN + "!");
			} else {
				play.sendMessage(ChatColor.GREEN + "This warp is in an unloaded world, please contact staff!");
			}
		}
		if(e.getSlot() == 14){
			play.closeInventory();
		}
		if(e.getSlot() == 26){
			play.closeInventory();
			profile.getKnownWarps().remove(warp.getId());
			play.sendMessage(ChatColor.YELLOW + "You have forgotten the warp " + warp.getName() + ChatColor.YELLOW + "!");
		}
	}

}
