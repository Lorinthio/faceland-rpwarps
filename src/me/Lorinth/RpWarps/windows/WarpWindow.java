package me.Lorinth.RpWarps.windows;

import static me.Lorinth.RpWarps.RpWarpsMain.PROFILES;
import java.util.ArrayList;
import java.util.HashMap;

import me.Lorinth.RpWarps.PlayerWarpProfile;
import me.Lorinth.RpWarps.RpWarpsMain;
import me.Lorinth.RpWarps.Warp;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class WarpWindow {

	private Inventory inv;
	private Player window_player;
	private Player command_player;
	private RpWarpsMain main;
	private HashMap<Integer, Warp> windowWarps = new HashMap<Integer, Warp>();
	private PlayerWarpProfile profile;
	private int warpIconsPlaced;
	private boolean samePlayer;

	public WarpWindow(RpWarpsMain main, Player window_player, Player command_player){
		this.main = main;
		this.window_player = window_player;
		this.command_player = command_player;
		this.profile = PROFILES.get(window_player);

		createWindow();
	}

	private void createWindow(){
		samePlayer = (window_player == command_player);
		if(samePlayer) {
			command_player.sendMessage(ChatColor.GREEN + "Opening teleport menu!");
			inv = Bukkit.createInventory(null, 45, ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Title Color")) + main.configYml.getString("Title"));
		} else {
			command_player.sendMessage(ChatColor.RED + "Opening " + ChatColor.WHITE + window_player.getName() + ChatColor.RED + "'s teleport menu!");
			inv = Bukkit.createInventory(null, 45, ChatColor.translateAlternateColorCodes('&', main.configYml.getString("Title Color")) + main.configYml.getString("Title"));
		}

		int slot = 0;

		warpIconsPlaced = 0;

		ArrayList<String> remove = new ArrayList<String>();
		ArrayList<Warp> bannedFrom = new ArrayList<Warp>();
		for (String id : profile.getKnownWarps()) {
			Warp warp = main.warpUtils.getWarp(id);
			if (warp != null) {
				if (warp.getBannedUsers().contains(window_player.getUniqueId().toString())) {
					bannedFrom.add(warp);
				} else {
					if(warpIconsPlaced < 45){
						ItemStack warpIcon = new ItemStack(warp.getDisplayItem());
						warpIcon.setDurability(warp.getDurability());
						inv.setItem(slot, warpIcon);

						windowWarps.put(slot, warp);
						slot ++;
					}
					warpIconsPlaced++;
				}
			} else {
				remove.add(id);
			}
		}
		for (Warp w : bannedFrom) {
			//Remove known warp from user profile
			profile.getKnownWarps().remove(w.getId());
		}
		profile.getKnownWarps().removeAll(remove);

		command_player.openInventory(inv);
	}

	public void HandleClickEvent(InventoryClickEvent event) {
		if (inv.getSize() > event.getSlot() && event.getSlot() >= 0) {
			ItemStack item = inv.getItem(event.getSlot());
			if (item != null) {
				command_player.closeInventory();

				Warp warp = windowWarps.get(event.getSlot());

				RpWarpsMain.winMan.addConfirmWindow(command_player, new WarpConfirmWindow(command_player, warp, profile, samePlayer));
			}
		}
	}
}
